package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeThrees, uniqueValues and addList.
 * These programming tasks was part of lab5 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/5/
 */
public class Lab5 {
    
    public static void main(String[] args) {

    }

    public static ArrayList<Integer> multipliedWithTwo(ArrayList<Integer> list) {
        ArrayList<Integer> multiplied = new ArrayList<>();
        for (Integer i : list) {
            multiplied.add(i*2);
        }
        return multiplied;
    }

    public static ArrayList<Integer> removeThrees(ArrayList<Integer> list) {
        ArrayList<Integer> removed = new ArrayList<>();
        for (Integer i : list) {
            if (i != 3) {
                removed.add(i);
            }
        }
        return removed;
    }

    public static ArrayList<Integer> uniqueValues(ArrayList<Integer> list) {
        ArrayList<Integer> unique = new ArrayList<>();
        for (Integer i : list) {
            if (!unique.contains(i)) {
                unique.add(i);
            }
        }
        return unique;
    }

    public static void addList(ArrayList<Integer> a, ArrayList<Integer> b) {
        for (int i = 0; i < b.size(); i++) {
            a.set(i, (a.get(i) + b.get(i)));
        }
    }
}