package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        // Call the methods here to test them on different inputs
        
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int w1 = word1.length();
        int w2 = word2.length();
        int w3 = word3.length();
        int max = Math.max(w1, Math.max(w2, w3));

        if (w1 == max) {
            System.out.println(word1);
        }
        if (w2 == max) {
            System.out.println(word2);
        }
        if (w3 == max) {
            System.out.println(word3);
        }
    }

    public static boolean isLeapYear(int year) {
        if (year % 100 == 0 && year % 400 == 0) {
            return true;
        } else if (year % 100 == 0) {
            return false;
        } else if (year % 4 == 0) {
            return true;
        } else return false;
    }

    public static boolean isEvenPositiveInt(int num) {
        return num % 2 == 0 && num > 0;
    }

}
