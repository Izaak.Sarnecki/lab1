package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {

    }

    public static void multiplesOfSevenUpTo(int n) {
        int i = 7;
        while (i <= n) {
            System.out.println(i);
            i += 7;
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(j*i + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        String numString = ((Integer) num).toString();
        ArrayList<String> numList = new ArrayList<>(Arrays.asList(numString.split("")));
        int crossSum = 0;
        for (String c : numList) {
            crossSum += Integer.parseInt(c);
        }
        return crossSum;
    }

}