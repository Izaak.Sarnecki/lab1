package INF101.lab1.INF100labs;

import java.util.ArrayList;

/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        // Call the methods here to test them on different inputs

    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        int sumRow = 0;
        int sumCol = 0;

        for (Integer dig : grid.get(0)) {
            sumRow += dig;
        }
        for (int i = 0; i < grid.size(); i++) {
            sumCol += grid.get(i).get(0);
        }

        for (int i = 1; i < grid.size(); i++) {
            int tempCol = 0;
            int tempRow = 0;
            for (int j = 0; j < grid.get(i).size(); j++) {
                tempRow += grid.get(i).get(j);
                tempCol += grid.get(j).get(i);
            }
            if (tempRow != sumRow || tempCol != sumCol) {
                return false;
            }
        }
        return true;
        }
    }
