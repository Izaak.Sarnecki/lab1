package INF101.lab1.rockPaperScissors;

import java.util.List;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;


public class RockPaperScissors {
	
    public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        boolean play = true;
        while (play) {
            System.out.println("Let's play round " + roundCounter);
            String input = getUserInputWeapon();

            Random rand = new Random();
            String computerChoice = rpsChoices.get(rand.nextInt(rpsChoices.size()));
            String outcome = play(input, computerChoice);

            displayOutcome(input, computerChoice, outcome);
            roundCounter++;

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            boolean playContinue = getContinue();
            if (!playContinue) {
                play = false;
            }
        }
        System.out.println("Bye bye :)");
    }

    public String getUserInputWeapon() {
        String input;
        while (true) {
            input = readInput("Your choice (Rock/Paper/Scissors)? ").toLowerCase();
            if (!rpsChoices.contains(input)) {
                System.out.println("I do not understand " + input + ". Could you try again?");
            } else return input;
        }
    }

    public boolean getContinue() {
        List<String> cont = Arrays.asList("y", "n");
        String input;
        while (true) {
            input = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            if (!cont.contains(input)) {
                System.out.println("I do not understand " + input + ". Could you try again?");
            } else break;

        }
        return input.equals("y");
    }

    public static String play(String input, String computer) {
        if (input.equals(computer)) {
            return "t";
        } else if ((input.equals("rock") && computer.equals("scissors"))
                || input.equals("scissors") && computer.equals("paper")) {
            return "w";
        } else return "l";
    }

    private  void displayOutcome(String input, String computerChoice, String outcome) {
        System.out.print("Human chose " + input + ", computer chose " + computerChoice + ".");
        if (outcome.equals("t")) {
            System.out.println(" It's a tie! ");
        } else if (outcome.equals("w")) {
            humanScore++;
            System.out.println(" Human wins!");
        } else {
            computerScore++;
            System.out.println(" Computer wins!");
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        return sc.next();
    }

}